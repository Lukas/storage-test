#!/usr/bin/env python3

with open("./differences", "rb") as f:
  blocks = 0
  errors = 0
  error_bits = 0
  good = 0
  max_good = 0
#  failure_map = []
#  failures = 0
  b = f.read(512)
  while len(b) == 512:
    blocks += 1
#    failures *= 2
    if b != 512 * b'\0':
      errors += 1
      good = 0
#      for by in b:
#        for bit in range(0, 8):
#          if by & (1 << bit):
#            error_bits += 1
#      failures += 1
#    if blocks % 8 == 0:
#      failure_map += [failures]
#      failures = 0
    else:
      good += 1
      if good > max_good:
        max_good = good
        good_pos = blocks - good + 1
    b = f.read(512)
    if blocks % 100000 == 0:
      print("I'm at " + str(blocks) + " blocks")
  if len(b) != 0:
    print("Unexpected length: " + str(len(b)))
  print("Errors in " + str(errors) + " of " + str(blocks) + " blocks.")
  percent = error_bits / errors / 512 / 8 * 100
  print(str(percent) + " % der Bits in fehlerhaften Sektoren waren gekippt.")
  print("Ab Sektor " + str(good_pos) + " sind " + str(max_good) + " zusammenhängende Sektoren fehlerfrei (größter Bereich).")
#  with open("./failure_map", "wb") as f:
#    f.write(bytes(failure_map))
