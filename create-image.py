#!/usr/bin/env python3
overhead = b"BM\xda'\xd0\x02"+4*b"\x00"+b"z"+3*b"\x00"+b"l"+3*b"\x00"+b"\xa0\x0f"+2*b"\x00"+b"]\x0f"+2*b"\x00"+b"\x01\x00\x18"+5*b"\x00"+b"`'\xd0\x02\x13\x0b"+2*b"\x00"+b"\x13\x0b"+10*b"\x00"+b"BGRs"+48*b"\x00"+b"\x02"+15*b"\x00"

def getbit(filecontent, pos):
  byte_pos = pos//8
  bitpos = pos%8
  if byte_pos >= len(filecontent):
    return False
  return (filecontent[byte_pos] & (1<<(7-bitpos))) != 0

with open("./failure_map", "rb") as f:
  map_file = f.read()
  print("Soll: 1966080, Ist: " + str(len(map_file)))

with open("./map.bmp", "wb") as f:
  f.write(overhead)
  for y in range(3932, -1, -1):
    for x in range(0, 4000):
      bitpos = y * 4000 + x
      if getbit(map_file, bitpos):
        f.write(3*b"\x00")
      else:
        f.write(3*b"\xff")
